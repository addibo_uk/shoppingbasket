﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ShoppingBasket.EntityModels
{
    public class OfferVoucherEntity
    {
        [Key]
        public Guid OfferVoucherId { get; set; }
        public string OfferVoucherCode { get; set; }
        public string OfferVoucherDescription { get; set; }
        public decimal OfferVoucherThresholdValue { get; set; }
        public decimal OfferVoucherValue { get; set; }
        public Guid? ProductCategoryId { get; set; }

        [ForeignKey(nameof(ProductCategoryId))]
        public virtual ProductCategoryEntity ProductCategory { get; set; }
    }
}
