﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ShoppingBasket.EntityModels
{
    public class GiftVoucherEntity
    {
        [Key]
        public Guid GiftVoucherId { get; set; }
        public string GiftVoucherCode { get; set; }
        public decimal GiftVoucherValue { get; set; }
    }
}
