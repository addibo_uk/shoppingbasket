﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ShoppingBasket.EntityModels
{
    public class ProductCategoryEntity
    {
        [Key]
        public Guid CategoryId { get; set; }
        public string CategoryDescription { get; set; }
    }
}
