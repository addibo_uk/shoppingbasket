﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ShoppingBasket.EntityModels
{
    public class ProductEntity
    {
        [Key]
        public Guid ProductId { get; set; }
        public string ProductDescription { get; set; }
        public decimal ProductPrice { get; set; }
        public Guid ProductCategoryId { get; set; }

        [ForeignKey(nameof(ProductCategoryId))]
        public virtual ProductCategoryEntity ProductCategory { get; set; }
    }
}
