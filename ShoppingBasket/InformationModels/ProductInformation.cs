﻿using System;

namespace ShoppingBasket.InformationModels
{
    public class ProductInformation
    {
        public Guid ProductId { get; set; }
        public string ProductDescription { get; set; }
        public decimal ProductPrice { get; set; }
        public ProductCategoryInformation ProductCategory { get; set; }
    }
}
