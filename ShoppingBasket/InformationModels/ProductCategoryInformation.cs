﻿using System;

namespace ShoppingBasket.InformationModels
{
    public class ProductCategoryInformation
    {
        public Guid CategoryId { get; set; }
        public string CategoryDescription { get; set; }
    }
}
