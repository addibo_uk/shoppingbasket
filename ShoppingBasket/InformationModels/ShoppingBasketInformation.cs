﻿using System.Collections.Generic;
using System.Linq;

namespace ShoppingBasket.InformationModels
{
    public class ShoppingBasketInformation
    {
        public IEnumerable<ShoppingBasketItemInformation> Items { get; set; } = Enumerable.Empty<ShoppingBasketItemInformation>();
        public decimal BasketTotal { get; set; }
        public decimal BasketDiscountedTotal { get; set; }
        public string Status { get; set; } = string.Empty;
    }
}
