﻿using System;

namespace ShoppingBasket.InformationModels
{
    public class OfferVoucherInformation
    {
        public Guid OfferVoucherId { get; set; }
        public string OfferVoucherCode { get; set; }
        public string OfferVoucherDescription { get; set; }
        public decimal OfferVoucherThresholdValue { get; set; }
        public decimal OfferVoucherValue { get; set; }
        public ProductCategoryInformation ProductCategory { get; set; }
    }
}
