﻿namespace ShoppingBasket.InformationModels
{
    public class ShoppingBasketItemInformation
    {
        public ProductInformation Product { get; set; }
        public int ProductQuantity { get; set; }
    }
}
