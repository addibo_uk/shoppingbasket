﻿using System;

namespace ShoppingBasket.InformationModels
{
    public class NewShoppingBasketItemInformation
    {
        public Guid ProductId { get; set; }
        public int ProductQuantity { get; set; }
    }
}
