﻿using System.Collections.Generic;
using System.Linq;

namespace ShoppingBasket.InformationModels
{
    public class NewShoppingBasketInformation
    {
        public IEnumerable<NewShoppingBasketItemInformation> Items { get; set; } = Enumerable.Empty<NewShoppingBasketItemInformation>();
        public IEnumerable<string> VoucherCodes { get; set; } = Enumerable.Empty<string>();
        public string OfferCode { get; set; }
    }
}
