﻿using System;

namespace ShoppingBasket.InformationModels
{
    public class GiftVoucherInformation
    {
        public Guid GiftVoucherId { get; set; }
        public string GiftVoucherCode { get; set; }
        public decimal GiftVoucherValue { get; set; }
    }
}
