﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ShoppingBasket.Migrations
{
    public partial class VouchersAdded : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "GiftVouchers",
                columns: table => new
                {
                    GiftVoucherId = table.Column<Guid>(nullable: false),
                    GiftVoucherCode = table.Column<string>(nullable: true),
                    GiftVoucherValue = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GiftVouchers", x => x.GiftVoucherId);
                });

            migrationBuilder.CreateTable(
                name: "OfferVouchers",
                columns: table => new
                {
                    OfferVoucherId = table.Column<Guid>(nullable: false),
                    OfferVoucherCode = table.Column<string>(nullable: true),
                    OfferVoucherDescription = table.Column<string>(nullable: true),
                    OfferVoucherThresholdValue = table.Column<decimal>(nullable: false),
                    OfferVoucherValue = table.Column<decimal>(nullable: false),
                    ProductCategoryId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OfferVouchers", x => x.OfferVoucherId);
                    table.ForeignKey(
                        name: "FK_OfferVouchers_ProductCategories_ProductCategoryId",
                        column: x => x.ProductCategoryId,
                        principalTable: "ProductCategories",
                        principalColumn: "CategoryId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_OfferVouchers_ProductCategoryId",
                table: "OfferVouchers",
                column: "ProductCategoryId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "GiftVouchers");

            migrationBuilder.DropTable(
                name: "OfferVouchers");
        }
    }
}
