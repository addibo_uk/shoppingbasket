﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using ShoppingBasket.Data;
using ShoppingBasket.EntityModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ShoppingBasket.Extensions
{
    public static class DBExtensions
    {
        public static IWebHost SeedData(this IWebHost host)
        {
            using (var scope = host.Services.CreateScope())
            {
                var services = scope.ServiceProvider;
                var context = services.GetService<ShoppingBasketDbContext>();

                AddProductCategories(context);
                AddProducts(context);
                AddGiftVouchers(context);
                AddOfferVouchers(context);
            }
            return host;
        }

        public static void AddProductCategories(ShoppingBasketDbContext context)
        {
            if (!context.ProductCategories.Any())
            {
                var categories = new List<ProductCategoryEntity>
                {
                    new ProductCategoryEntity
                    {
                        CategoryId = Guid.Parse("97d7bdd9-0e4a-4f55-95e5-593ea7c149d2"),
                        CategoryDescription = "Gift Vouchers"
                    },
                    new ProductCategoryEntity
                    {
                        CategoryId = Guid.Parse("6d2b16e5-9e66-4181-b152-520f3b1c9a13"),
                        CategoryDescription = "Tops"
                    },
                    new ProductCategoryEntity
                    {
                        CategoryId = Guid.Parse("c962832d-dc03-4a04-8bcf-2fd3d7bf41a1"),
                        CategoryDescription = "Hats"
                    },
                    new ProductCategoryEntity
                    {
                        CategoryId = Guid.Parse("f9135480-e717-47a5-9fc1-869638785eeb"),
                        CategoryDescription = "Head Gear"
                    }
                };
                context.ProductCategories.AddRange(categories);
                context.SaveChanges();
            }
        }

        public static void AddProducts(ShoppingBasketDbContext context)
        {
            if (!context.Products.Any())
            {
                var products = new List<ProductEntity>
                {
                    new ProductEntity
                    {
                        ProductId = Guid.Parse("05e3c0f1-6db8-42f2-ba56-aad8ad01a0f5"),
                        ProductDescription = "Standard Hat",
                        ProductPrice = 10.50M,
                        ProductCategory = context.ProductCategories.Single(
                            category => category.CategoryDescription == "Hats")
                    },
                    new ProductEntity
                    {
                        ProductId = Guid.Parse("afb9987e-8604-438f-95e8-0bfd9b8cfde0"),
                        ProductDescription = "Expensive Hat",
                        ProductPrice = 25.00M,
                        ProductCategory = context.ProductCategories.Single(
                            category => category.CategoryDescription == "Hats")
                    },
                    new ProductEntity
                    {
                        ProductId = Guid.Parse("d164bf2e-7e5d-4efe-b776-c42c5f306fb3"),
                        ProductDescription = "Standard Jumper",
                        ProductPrice = 26.00M,
                        ProductCategory = context.ProductCategories.Single(
                            category => category.CategoryDescription == "Tops")
                    },
                    new ProductEntity
                    {
                        ProductId = Guid.Parse("a5b14daf-050a-4a3a-8cb1-252ba1035772"),
                        ProductDescription = "Expensive Jumper",
                        ProductPrice = 54.65M,
                        ProductCategory = context.ProductCategories.Single(
                            category => category.CategoryDescription == "Tops")
                    },
                    new ProductEntity
                    {
                        ProductId = Guid.Parse("28763fc8-866a-4c0c-addd-7eeaa2c854f0"),
                        ProductDescription = "Head Light",
                        ProductPrice = 3.50M,
                        ProductCategory = context.ProductCategories.Single(
                            category => category.CategoryDescription == "Head Gear")
                    },
                    new ProductEntity
                    {
                        ProductId = Guid.Parse("5f10bd73-3c1e-4d8f-9435-7c7956e0f973"),
                        ProductDescription = "Gift Voucher",
                        ProductPrice = 30.00M,
                        ProductCategory = context.ProductCategories.Single(
                            category => category.CategoryDescription == "Gift Vouchers")
                    }
                };
                context.Products.AddRange(products);
                context.SaveChanges();
            }
        }

        public static void AddGiftVouchers(ShoppingBasketDbContext context)
        {
            if (!context.GiftVouchers.Any())
            {
                var giftVouchers = new List<GiftVoucherEntity>
                {
                    new GiftVoucherEntity
                    {
                        GiftVoucherId = Guid.Parse("9860b642-615f-4063-9c2b-5f14cacefb30"),
                        GiftVoucherCode = "XXX-XXX",
                        GiftVoucherValue = 5.00M
                    }
                };

                context.GiftVouchers.AddRange(giftVouchers);
                context.SaveChanges();
            }
        }

        public static void AddOfferVouchers(ShoppingBasketDbContext context)
        {
            if (!context.OfferVouchers.Any())
            {
                var offerVouchers = new List<OfferVoucherEntity>
                {
                    new OfferVoucherEntity
                    {
                        OfferVoucherId = Guid.Parse("76de2181-04c5-4589-a437-17ef10ba5bfc"),
                        OfferVoucherCode = "YYY-YYY",
                        OfferVoucherDescription= "£5.00 off Head Gear in baskets over £50.00",
                        OfferVoucherThresholdValue = 50.00M,
                        OfferVoucherValue = 5.00M,
                        ProductCategory = context.ProductCategories.Single(
                            category => category.CategoryDescription == "Head Gear")
                    },
                    new OfferVoucherEntity
                    {
                        OfferVoucherId = Guid.Parse("fbedb18a-07a6-4c98-be09-6ffbb52bb8b6"),
                        OfferVoucherCode = "ZZZ-ZZZ",
                        OfferVoucherDescription= "£5.00 off baskets over £50.00",
                        OfferVoucherThresholdValue = 50.00M,
                        OfferVoucherValue = 5.00M,
                        ProductCategory = null
                    }
                };

                context.OfferVouchers.AddRange(offerVouchers);
                context.SaveChanges();
            }
        }
    }
}