﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using ShoppingBasket.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShoppingBasket.Data
{
    public class ProductRepository : IProductRepository
    {
        private readonly ShoppingBasketDbContext _dbContext;
        private readonly IMapper _mapper;

        public ProductRepository(ShoppingBasketDbContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }

        public async Task<IEnumerable<ProductDomainModel>> GetAllProductsAsync() =>
            (await _dbContext.Products
                   .Include(product => product.ProductCategory)
                   .ToListAsync())
                   .Select(_mapper.Map<ProductDomainModel>);

        public async Task<IEnumerable<ProductDomainModel>> GetProductsAsync(IEnumerable<Guid> productIds) =>
            (await _dbContext.Products
                   .Include(product => product.ProductCategory)
                   .Where(product => productIds.Contains(product.ProductId))
                   .ToListAsync())
                   .Select(_mapper.Map<ProductDomainModel>);
    }
}
