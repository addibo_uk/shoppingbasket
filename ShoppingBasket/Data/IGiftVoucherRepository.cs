﻿using ShoppingBasket.DomainModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ShoppingBasket.Data
{
    public interface IGiftVoucherRepository
    {
        Task<GiftVoucherDomainModel> GetGiftVoucherAsync(string voucherCode);
        Task<IEnumerable<GiftVoucherDomainModel>> GetGiftVouchersAsync(IEnumerable<string> voucherCodes);
    }
}
