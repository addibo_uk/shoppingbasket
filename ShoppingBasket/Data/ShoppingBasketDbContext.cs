﻿using Microsoft.EntityFrameworkCore;
using ShoppingBasket.EntityModels;

namespace ShoppingBasket.Data
{
    public class ShoppingBasketDbContext : DbContext
    {
        public ShoppingBasketDbContext(DbContextOptions<ShoppingBasketDbContext> options)
            : base(options)
        { }

        public DbSet<ProductEntity> Products { get; set; }
        public DbSet<ProductCategoryEntity> ProductCategories { get; set; }
        public DbSet<GiftVoucherEntity> GiftVouchers { get; set; }
        public DbSet<OfferVoucherEntity> OfferVouchers { get; set; }
    }
}
