﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using ShoppingBasket.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShoppingBasket.Data
{
    public class GiftVoucherRepository : IGiftVoucherRepository
    {
        private readonly ShoppingBasketDbContext _dbContext;
        private readonly IMapper _mapper;

        public GiftVoucherRepository(ShoppingBasketDbContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }

        public async Task<GiftVoucherDomainModel> GetGiftVoucherAsync(string voucherCode) =>
            (await _dbContext.GiftVouchers
                   .Where(voucher => voucher.GiftVoucherCode.ToLower() == voucherCode.ToLower())
                   .ToListAsync())
                   .Select(_mapper.Map<GiftVoucherDomainModel>).SingleOrDefault();

        public async Task<IEnumerable<GiftVoucherDomainModel>> GetGiftVouchersAsync(IEnumerable<string> voucherCodes) =>
            (await _dbContext.GiftVouchers
                   .Where(voucher => voucherCodes.Contains(voucher.GiftVoucherCode, StringComparer.InvariantCultureIgnoreCase))
                   .ToListAsync())
                   .Select(_mapper.Map<GiftVoucherDomainModel>);
    }
}
