﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using ShoppingBasket.DomainModels;
using System.Linq;
using System.Threading.Tasks;

namespace ShoppingBasket.Data
{
    public class OfferVoucherRepository : IOfferVoucherRepository
    {
        private readonly ShoppingBasketDbContext _dbContext;
        private readonly IMapper _mapper;

        public OfferVoucherRepository(ShoppingBasketDbContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }

        public async Task<OfferVoucherDomainModel> GetOfferVoucherAsync(string voucherCode) =>
            (await _dbContext.OfferVouchers
                   .Include(voucher => voucher.ProductCategory)
                   .Where(voucher => voucher.OfferVoucherCode.ToLower() == voucherCode.ToLower())
                   .ToListAsync())
                   .Select(_mapper.Map<OfferVoucherDomainModel>).SingleOrDefault();
    }
}
