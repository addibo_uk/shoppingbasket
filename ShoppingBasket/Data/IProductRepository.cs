﻿using ShoppingBasket.DomainModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ShoppingBasket.Data
{
    public interface IProductRepository
    {
        Task<IEnumerable<ProductDomainModel>> GetAllProductsAsync();
        Task<IEnumerable<ProductDomainModel>> GetProductsAsync(IEnumerable<Guid> productIds);
    }
}
