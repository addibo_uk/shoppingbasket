﻿using ShoppingBasket.DomainModels;
using System.Threading.Tasks;

namespace ShoppingBasket.Data
{
    public interface IOfferVoucherRepository
    {
        Task<OfferVoucherDomainModel> GetOfferVoucherAsync(string voucherCode);
    }
}
