﻿using FluentValidation;
using FluentValidation.Results;
using ShoppingBasket.InformationModels;

namespace ShoppingBasket.Validators
{
    public class NewShoppingBasketInformationValidator : AbstractValidator<NewShoppingBasketInformation>
    {
        public NewShoppingBasketInformationValidator()
        {
            RuleFor(basket => basket.Items)
                .NotEmpty();
        }

        protected override bool PreValidate(ValidationContext<NewShoppingBasketInformation> context, ValidationResult result)
        {
            if (context.InstanceToValidate != null) return true;

            result.Errors.Add(new ValidationFailure("", $"{nameof(NewShoppingBasketInformation)} is null"));

            return false;
        }
    }
}
