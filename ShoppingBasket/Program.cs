﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using ShoppingBasket.Extensions;

namespace ShoppingBasket
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().SeedData().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();
    }
}
