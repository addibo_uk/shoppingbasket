using AutoMapper;
using FluentValidation;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ShoppingBasket.Data;
using ShoppingBasket.InformationModels;
using ShoppingBasket.Services;
using ShoppingBasket.Validators;

namespace ShoppingBasket
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ShoppingBasketDbContext>(options =>
                options.UseSqlite(Configuration.GetConnectionString("DefaultConnectionString")));

            services.AddAutoMapper();
            services.AddMvc()
                .AddFluentValidation()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddScoped<IProductRepository, ProductRepository>();
            services.AddScoped<IProductService, ProductService>();
            services.AddScoped<IOfferVoucherRepository, OfferVoucherRepository>();
            services.AddScoped<IOfferVoucherService, OfferVoucherService>();
            services.AddScoped<IGiftVoucherService, GiftVoucherService>();
            services.AddScoped<IGiftVoucherRepository, GiftVoucherRepository>();
            services.AddScoped<IValidator<NewShoppingBasketInformation>, NewShoppingBasketInformationValidator>();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
