﻿using ShoppingBasket.Data;
using ShoppingBasket.DomainModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ShoppingBasket.Services
{
    public class ProductService : IProductService
    {
        private readonly IProductRepository _productRepository;

        public ProductService(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }

        public async Task<IEnumerable<ProductDomainModel>> GetAllProductsAsync() =>
           await _productRepository.GetAllProductsAsync();

        public async Task<IEnumerable<ProductDomainModel>> GetProductsAsync(IEnumerable<Guid> productIds) =>
           await _productRepository.GetProductsAsync(productIds);
    }
}
