﻿using ShoppingBasket.DomainModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ShoppingBasket.Services
{
    public interface IGiftVoucherService
    {
        Task<GiftVoucherDomainModel> GetGiftVoucherAsync(string voucherCode);
        Task<IEnumerable<GiftVoucherDomainModel>> GetGiftVouchersAsync(IEnumerable<string> voucherCodes);
    }
}
