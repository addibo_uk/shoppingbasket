﻿using ShoppingBasket.Data;
using ShoppingBasket.DomainModels;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ShoppingBasket.Services
{
    public class OfferVoucherService : IOfferVoucherService
    {
        private readonly IOfferVoucherRepository _offerVoucherRepository;

        public OfferVoucherService(IOfferVoucherRepository offerVoucherRepository)
        {
            _offerVoucherRepository = offerVoucherRepository;
        }

        public async Task<OfferVoucherDomainModel> GetOfferVoucherAsync(string voucherCode) =>
            await _offerVoucherRepository.GetOfferVoucherAsync(voucherCode);

        public async Task<ProcessedOfferVoucherDomainModel> ProcessOfferVoucher(string voucherCode, ShoppingBasketDomainModel basket)
        {
            if (string.IsNullOrEmpty(voucherCode) || !basket.Items.Any())
                return new ProcessedOfferVoucherDomainModel();

            var offerVoucher = await GetOfferVoucherAsync(voucherCode);
            if (offerVoucher == null)
            {
                return new ProcessedOfferVoucherDomainModel
                {
                    DiscountStatus = $"'{voucherCode}' is not a valid voucher code"
                };
            }

            var productValueDifference = 0.00M;
            if (offerVoucher.ProductCategory != null)
            {
                var qualifyingProducts = basket.Items
                    .Where(item => item.Product.ProductCategory.CategoryId == offerVoucher.ProductCategory.CategoryId).ToList();

                if (!qualifyingProducts.Any())
                {
                    return new ProcessedOfferVoucherDomainModel
                    {
                        DiscountStatus = $"There are no qualifying products in your basket, needed to use voucher '{voucherCode}'"
                    };
                }

                productValueDifference = qualifyingProducts.Sum(item => item.Product.ProductPrice * item.ProductQuantity) - offerVoucher.OfferVoucherValue;
                productValueDifference = productValueDifference < 0 ? Math.Abs(productValueDifference) : 0; 
            }

            var giftVoucherAmount = basket.Items
                .Where(item => item.Product.ProductCategory.CategoryDescription == "Gift Vouchers")
                .Sum(voucher => voucher.Product.ProductPrice * voucher.ProductQuantity);

            if (basket.BasketTotal - giftVoucherAmount >= offerVoucher.OfferVoucherThresholdValue)
            {
                return new ProcessedOfferVoucherDomainModel
                {
                    DiscountValue = (basket.BasketTotal - offerVoucher.OfferVoucherValue) + productValueDifference
                };
            }

            return new ProcessedOfferVoucherDomainModel
            {
                DiscountStatus = $"You have not reached the required spend threshold for voucher {voucherCode}. " +
                    $"You need to spend another £{string.Format("{0:0.00}", offerVoucher.OfferVoucherThresholdValue - (basket.BasketTotal - giftVoucherAmount) + 0.01M)} " +
                    $"to receive a £{string.Format("{0:0.00}", offerVoucher.OfferVoucherValue)} discount off of your basket total."
            };
        }
    }
}
