﻿using ShoppingBasket.DomainModels;
using System.Threading.Tasks;

namespace ShoppingBasket.Services
{
    public interface IOfferVoucherService
    {
        Task<OfferVoucherDomainModel> GetOfferVoucherAsync(string voucherCode);
        Task<ProcessedOfferVoucherDomainModel> ProcessOfferVoucher(string voucherCode, ShoppingBasketDomainModel basket);
    }
}
