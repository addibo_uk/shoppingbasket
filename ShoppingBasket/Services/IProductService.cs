﻿using ShoppingBasket.DomainModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ShoppingBasket.Services
{
    public interface IProductService
    {
        Task<IEnumerable<ProductDomainModel>> GetAllProductsAsync();
        Task<IEnumerable<ProductDomainModel>> GetProductsAsync(IEnumerable<Guid> productIds);
    }
}
