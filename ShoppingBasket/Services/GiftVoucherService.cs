﻿using ShoppingBasket.Data;
using ShoppingBasket.DomainModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ShoppingBasket.Services
{
    public class GiftVoucherService : IGiftVoucherService
    {
        private readonly IGiftVoucherRepository _giftVoucherRepository;

        public GiftVoucherService(IGiftVoucherRepository giftVoucherRepository)
        {
            _giftVoucherRepository = giftVoucherRepository;
        }

        public async Task<GiftVoucherDomainModel> GetGiftVoucherAsync(string voucherCode) =>
            await _giftVoucherRepository.GetGiftVoucherAsync(voucherCode);

        public async Task<IEnumerable<GiftVoucherDomainModel>> GetGiftVouchersAsync(IEnumerable<string> voucherCodes) =>
           await _giftVoucherRepository.GetGiftVouchersAsync(voucherCodes);
    }
}
