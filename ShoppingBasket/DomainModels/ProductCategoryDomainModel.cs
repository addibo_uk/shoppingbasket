﻿using System;

namespace ShoppingBasket.DomainModels
{
    public class ProductCategoryDomainModel
    {
        public Guid CategoryId { get; set; }
        public string CategoryDescription { get; set; }
    }
}
