﻿using System;

namespace ShoppingBasket.DomainModels
{
    public class ProductDomainModel
    {
        public Guid ProductId { get; set; }
        public string ProductDescription { get; set; }
        public decimal ProductPrice { get; set; }
        public ProductCategoryDomainModel ProductCategory { get; set; }
    }
}
