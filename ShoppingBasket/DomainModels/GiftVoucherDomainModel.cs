﻿using System;

namespace ShoppingBasket.DomainModels
{
    public class GiftVoucherDomainModel
    {
        public Guid GiftVoucherId { get; set; }
        public string GiftVoucherCode { get; set; }
        public decimal GiftVoucherValue { get; set; }
    }
}
