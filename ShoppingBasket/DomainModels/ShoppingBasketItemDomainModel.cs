﻿namespace ShoppingBasket.DomainModels
{
    public class ShoppingBasketItemDomainModel
    {
        public ProductDomainModel Product { get; set; }
        public int ProductQuantity { get; set; }
    }
}
