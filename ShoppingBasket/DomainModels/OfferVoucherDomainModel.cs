﻿using System;

namespace ShoppingBasket.DomainModels
{
    public class OfferVoucherDomainModel
    {
        public Guid OfferVoucherId { get; set; }
        public string OfferVoucherCode { get; set; }
        public string OfferVoucherDescription { get; set; }
        public decimal OfferVoucherThresholdValue { get; set; }
        public decimal OfferVoucherValue { get; set; }
        public ProductCategoryDomainModel ProductCategory { get; set; }
    }
}
