﻿namespace ShoppingBasket.DomainModels
{
    public class ProcessedOfferVoucherDomainModel
    {
        public decimal DiscountValue { get; set; } = 0.00M;
        public string DiscountStatus { get; set; } = string.Empty;
    }
}
