﻿using System.Collections.Generic;
using System.Linq;

namespace ShoppingBasket.DomainModels
{
    public class ShoppingBasketDomainModel
    {
        public IEnumerable<ShoppingBasketItemDomainModel> Items { get; set; } = Enumerable.Empty<ShoppingBasketItemDomainModel>();
        public decimal BasketTotal { get; set; } = 0.00M;
        public decimal BasketDiscountedTotal { get; set; } = 0.00M;
        public string Status { get; set; } = string.Empty;
    }
}
