﻿using AutoMapper;
using ShoppingBasket.DomainModels;
using ShoppingBasket.EntityModels;
using ShoppingBasket.InformationModels;

namespace ShoppingBasket.Mapping
{
    public class OfferVoucherMaps : Profile
    {
        public OfferVoucherMaps()
        {
            CreateMap<OfferVoucherEntity, OfferVoucherDomainModel>();
            CreateMap<OfferVoucherDomainModel, OfferVoucherInformation>();
        }
    }
}
