﻿using AutoMapper;
using ShoppingBasket.DomainModels;
using ShoppingBasket.EntityModels;
using ShoppingBasket.InformationModels;

namespace ShoppingBasket.Mapping
{
    public class ProductCategoryMaps : Profile
    {
        public ProductCategoryMaps()
        {
            CreateMap<ProductCategoryEntity, ProductCategoryDomainModel>();
            CreateMap<ProductCategoryDomainModel, ProductCategoryInformation>();
        }
    }
}
