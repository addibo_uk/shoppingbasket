﻿using AutoMapper;
using ShoppingBasket.DomainModels;
using ShoppingBasket.EntityModels;
using ShoppingBasket.InformationModels;

namespace ShoppingBasket.Mapping
{
    public class ProductMaps : Profile
    {
        public ProductMaps()
        {
            CreateMap<ProductEntity, ProductDomainModel>();
            CreateMap<ProductDomainModel, ProductInformation>();
        }
    }
}
