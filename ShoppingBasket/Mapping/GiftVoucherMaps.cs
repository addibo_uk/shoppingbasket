﻿using AutoMapper;
using ShoppingBasket.DomainModels;
using ShoppingBasket.EntityModels;
using ShoppingBasket.InformationModels;

namespace ShoppingBasket.Mapping
{
    public class GiftVoucherMaps : Profile
    {
        public GiftVoucherMaps()
        {
            CreateMap<GiftVoucherEntity, GiftVoucherDomainModel>();
            CreateMap<GiftVoucherDomainModel, GiftVoucherInformation>();
        }
    }
}
