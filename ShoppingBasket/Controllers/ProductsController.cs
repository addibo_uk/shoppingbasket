﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using ShoppingBasket.InformationModels;
using ShoppingBasket.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShoppingBasket.Controllers
{
    [ApiController]
    [Produces("application/json")]
    [Route("api/v1/[controller]")]
    public class ProductsController : ControllerBase
    {
        private readonly IProductService _productService;
        private readonly IMapper _mapper;

        public ProductsController(IProductService productService, IMapper mapper)
        {
            _productService = productService;
            _mapper = mapper;
        }
 
        public async Task<ActionResult<IEnumerable<ProductInformation>>> GetProducts(IEnumerable<Guid> productIds) =>
            (await _productService.GetProductsAsync(productIds)).Select(_mapper.Map<ProductInformation>).ToList();

        [HttpGet("all")]
        public async Task<ActionResult<IEnumerable<ProductInformation>>> GetAllProducts() =>
            (await _productService.GetAllProductsAsync()).Select(_mapper.Map<ProductInformation>).ToList();
    }
}
