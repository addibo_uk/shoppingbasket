﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using ShoppingBasket.InformationModels;
using ShoppingBasket.Services;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShoppingBasket.Controllers
{
    [ApiController]
    [Produces("application/json")]
    [Route("api/v1/[controller]")]
    public class GiftVouchersController : ControllerBase
    {
        private readonly IGiftVoucherService _giftVoucherService;
        private readonly IMapper _mapper;

        public GiftVouchersController(IGiftVoucherService giftVoucherService, IMapper mapper)
        {
            _giftVoucherService = giftVoucherService;
            _mapper = mapper;
        }

        public async Task<ActionResult<IEnumerable<GiftVoucherInformation>>> GetGiftVouchers(IEnumerable<string> voucherCodes) =>
            (await _giftVoucherService.GetGiftVouchersAsync(voucherCodes)).Select(_mapper.Map<GiftVoucherInformation>).ToList();

        [HttpGet("{voucherCode}")]
        public async Task<ActionResult<GiftVoucherInformation>> GetGiftVoucher(string voucherCode)
        {
            var voucher = _mapper.Map<GiftVoucherInformation>(await _giftVoucherService.GetGiftVoucherAsync(voucherCode ?? string.Empty));

            if (voucher == null)
                return NotFound($"'{voucherCode}' is not a valid voucher code");

            return voucher;
        }
    }
}
