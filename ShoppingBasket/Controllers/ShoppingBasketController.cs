﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using ShoppingBasket.DomainModels;
using ShoppingBasket.InformationModels;
using ShoppingBasket.Services;
using System.Linq;
using System.Threading.Tasks;

namespace ShoppingBasket.Controllers
{
    [ApiController]
    [Produces("application/json")]
    [Route("api/v1/[controller]")]
    public class ShoppingBasketController : ControllerBase
    {
        private readonly IProductService _productService;
        private readonly IOfferVoucherService _offerVoucherService;
        private readonly IGiftVoucherService _giftVoucherService;
        private readonly IMapper _mapper;

        public ShoppingBasketController(
            IProductService productService,
            IOfferVoucherService offerVoucherService,
            IGiftVoucherService giftVoucherService,
            IMapper mapper)
        {
            _productService = productService;
            _offerVoucherService = offerVoucherService;
            _giftVoucherService = giftVoucherService;
            _mapper = mapper;
        }

        [HttpPost]
        public async Task<ActionResult<ShoppingBasketInformation>> NewBasket(NewShoppingBasketInformation newBasket)
        {
            var products = await _productService.GetProductsAsync(
                newBasket.Items.Select(item => item.ProductId));

            var basketItems = products.Select(
                product => new ShoppingBasketItemInformation
                {
                    Product = _mapper.Map<ProductInformation>(product),
                    ProductQuantity = newBasket.Items.Single(item =>
                        item.ProductId == product.ProductId).ProductQuantity
                }).ToList();

            var basket = new ShoppingBasketInformation
            {
                Items = basketItems.AsEnumerable(),
                BasketTotal = basketItems.Sum(item => item.Product.ProductPrice * item.ProductQuantity)
            };

            var offerVoucherDiscount = await _offerVoucherService
                .ProcessOfferVoucher(newBasket.OfferCode, _mapper.Map<ShoppingBasketDomainModel>(basket));

            basket.BasketDiscountedTotal = offerVoucherDiscount.DiscountValue;

            var totalGiftVoucherValue = ((await _giftVoucherService.GetGiftVouchersAsync(newBasket.VoucherCodes))?
                .Sum(voucher => voucher.GiftVoucherValue)) ?? 0;

            basket.BasketDiscountedTotal = basket.BasketDiscountedTotal > 0 ?
                basket.BasketDiscountedTotal - totalGiftVoucherValue :
                basket.BasketTotal - totalGiftVoucherValue;

            if (basket.BasketDiscountedTotal < 0)
                basket.BasketDiscountedTotal = 0;

            basket.Status = offerVoucherDiscount.DiscountStatus;

            return basket;
        }
    }
}
