﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using ShoppingBasket.InformationModels;
using ShoppingBasket.Services;
using System.Threading.Tasks;

namespace ShoppingBasket.Controllers
{
    [ApiController]
    [Produces("application/json")]
    [Route("api/v1/[controller]")]
    public class OfferVouchersController : ControllerBase
    {
        private readonly IOfferVoucherService _offerVoucherService;
        private readonly IMapper _mapper;

        public OfferVouchersController(IOfferVoucherService offerVoucherService, IMapper mapper)
        {
            _offerVoucherService = offerVoucherService;
            _mapper = mapper;
        }

        [HttpGet("{voucherCode}")]
        public async Task<ActionResult<OfferVoucherInformation>> GetOfferVoucher(string voucherCode)
        {
            var voucher = _mapper.Map<OfferVoucherInformation>(await _offerVoucherService.GetOfferVoucherAsync(voucherCode ?? string.Empty));

            if (voucher == null)
                return NotFound($"'{voucherCode}' is not a valid voucher code");

            return voucher;
        }
    }
}
