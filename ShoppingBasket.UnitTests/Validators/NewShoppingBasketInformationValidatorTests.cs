﻿using FluentAssertions;
using ShoppingBasket.InformationModels;
using ShoppingBasket.Validators;
using System.Collections.Generic;
using Xunit;

namespace ShoppingBasket.UnitTests.Validators
{
    public class NewShoppingBasketInformationValidatorTests
    {
        private readonly NewShoppingBasketInformationValidator _validator;
        private readonly NewShoppingBasketInformation _shoppingBasketModel;

        public NewShoppingBasketInformationValidatorTests()
        {
            _validator = new NewShoppingBasketInformationValidator();
            _shoppingBasketModel = new NewShoppingBasketInformation
            {
                Items = new List<NewShoppingBasketItemInformation>
                {
                    new NewShoppingBasketItemInformation()
                }
            };
        }

        [Fact(DisplayName = "Given shopping basket model is valid When validate is invoked Then validation passes")]
        public void Validate_ModelIsValid_ThenValidationSucceeds()
        {
            var result = _validator.Validate(_shoppingBasketModel);
            result.IsValid.Should().BeTrue();
        }

        [Fact(DisplayName = "Given shopping basket model is null When validate is invoked Then validation fails")]
        public void Validate_NullModel_ThenValidationFails()
        {
            var result = _validator.Validate((NewShoppingBasketInformation)null);

            result.IsValid.Should().BeFalse();
        }

        [Fact(DisplayName = "Given shopping basket items list is empty When validate is invoked Then validation fails")]
        public void Validate_EmptyItems_ThenValidationFails()
        {
            _shoppingBasketModel.Items = new List<NewShoppingBasketItemInformation>();

            var result = _validator.Validate(_shoppingBasketModel);

            result.IsValid.Should().BeFalse();
        }
    }
}
