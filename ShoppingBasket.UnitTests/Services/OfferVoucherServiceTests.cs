﻿using FluentAssertions;
using Moq;
using ShoppingBasket.Data;
using ShoppingBasket.DomainModels;
using ShoppingBasket.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace ShoppingBasket.UnitTests.Services
{
    public class OfferVoucherServiceTests
    {
        private readonly Mock<IOfferVoucherRepository> _offerVoucherRepository;
        private readonly OfferVoucherService _offerVoucherService;

        private const string offerVoucherCode = "YYY-YYY";
        private const string offerVoucherDescription = "£5.00 off baskets over £50.00";
        private const decimal offerVoucherThreshold = 50.00M;
        private const decimal offerVoucherAmount = 10.00M;

        public OfferVoucherServiceTests()
        {
            _offerVoucherRepository = new Mock<IOfferVoucherRepository>();
            _offerVoucherService = new OfferVoucherService(_offerVoucherRepository.Object);

            _offerVoucherRepository.Setup(
                v => v.GetOfferVoucherAsync(It.IsAny<string>()))
                    .ReturnsAsync(new OfferVoucherDomainModel
                    {
                        OfferVoucherId = Guid.NewGuid(),
                        OfferVoucherCode = offerVoucherCode,
                        OfferVoucherDescription = offerVoucherDescription,
                        OfferVoucherThresholdValue = offerVoucherThreshold,
                        OfferVoucherValue = offerVoucherAmount,
                        ProductCategory = null
                    });
        }

        [Fact(DisplayName = @"Given offer voucher code When the basket value is greater than
            the voucher threshold Then it returns the correct discounted amount")]
        public async Task BasketIsGreaterThanVoucherThreshold_DeductVoucherValue()
        {
            var testBasket = new ShoppingBasketDomainModel
            {
                Items = new List<ShoppingBasketItemDomainModel>
                {
                    new ShoppingBasketItemDomainModel
                    {
                        Product = new ProductDomainModel
                        {
                            ProductId = Guid.NewGuid(),
                            ProductDescription = "Standard Hat",
                            ProductPrice = 10.00M,
                            ProductCategory = new ProductCategoryDomainModel
                            {
                                CategoryId = Guid.NewGuid(),
                                CategoryDescription = "Hats"
                            }
                        },
                        ProductQuantity = 6
                    }
                },
                BasketTotal = 60.00M,
                BasketDiscountedTotal = 60.00M,
                Status = string.Empty
            };

            var result = await _offerVoucherService.ProcessOfferVoucher(offerVoucherCode, testBasket);

            result.DiscountValue.Should().Be(testBasket.BasketTotal - offerVoucherAmount);
        }

        [Fact(DisplayName = @"Given offer voucher code When the basket value is less than
            the voucher threshold Then it returns a discounted amount that is equal to the basket amount")]
        public async Task BasketIsLessThanVoucherThreshold_DeductNothing()
        {
            var testBasket = new ShoppingBasketDomainModel
            {
                Items = new List<ShoppingBasketItemDomainModel>
                {
                    new ShoppingBasketItemDomainModel
                    {
                        Product = new ProductDomainModel
                        {
                            ProductId = Guid.NewGuid(),
                            ProductDescription = "Standard Hat",
                            ProductPrice = 10.00M,
                            ProductCategory = new ProductCategoryDomainModel
                            {
                                CategoryId = Guid.NewGuid(),
                                CategoryDescription = "Hats"
                            }
                        },
                        ProductQuantity = 1
                    }
                },
                BasketTotal = 10.00M,
                BasketDiscountedTotal = 10.00M,
                Status = string.Empty
            };

            var result = await _offerVoucherService.ProcessOfferVoucher(offerVoucherCode, testBasket);

            result.DiscountValue.Should().Be(0);
        }

        [Fact(DisplayName = @"Given offer voucher code When the basket value is greater than
            the voucher threshold and it contains a qualifying product Then it returns the correct discounted amount")]
        public async Task BasketIsGreaterThanVoucherThreshold_AndContainsValidProduct_DeductVoucherValue()
        {
            var testProductCategoryId = Guid.NewGuid();
            _offerVoucherRepository.Setup(
                v => v.GetOfferVoucherAsync(It.IsAny<string>()))
                    .ReturnsAsync(new OfferVoucherDomainModel
                    {
                        OfferVoucherId = Guid.NewGuid(),
                        OfferVoucherCode = offerVoucherCode,
                        OfferVoucherDescription = "£5.00 off Head Gear in baskets over £50.00",
                        OfferVoucherThresholdValue = offerVoucherThreshold,
                        OfferVoucherValue = offerVoucherAmount,
                        ProductCategory = new ProductCategoryDomainModel
                        {
                            CategoryId = testProductCategoryId,
                            CategoryDescription = "Head Gear"
                        }
                    });

            var testBasket = new ShoppingBasketDomainModel
            {
                Items = new List<ShoppingBasketItemDomainModel>
                {
                    new ShoppingBasketItemDomainModel
                    {
                        Product = new ProductDomainModel
                        {
                            ProductId = Guid.NewGuid(),
                            ProductDescription = "Standard Hat",
                            ProductPrice = 10.00M,
                            ProductCategory = new ProductCategoryDomainModel
                            {
                                CategoryId = Guid.NewGuid(),
                                CategoryDescription = "Hats"
                            }
                        },
                        ProductQuantity = 5
                    },
                    new ShoppingBasketItemDomainModel
                    {
                        Product = new ProductDomainModel
                        {
                            ProductId = Guid.NewGuid(),
                            ProductDescription = "Head Light",
                            ProductPrice = 3.50M,
                            ProductCategory = new ProductCategoryDomainModel
                            {
                                CategoryId = testProductCategoryId,
                                CategoryDescription = "Head Gear"
                            }
                        },
                        ProductQuantity = 1
                    }
                },
                BasketTotal = 53.50M,
                BasketDiscountedTotal = 53.50M,
                Status = string.Empty
            };

            var result = await _offerVoucherService.ProcessOfferVoucher(offerVoucherCode, testBasket);

            result.DiscountValue.Should().Be(
                testBasket.BasketTotal - testBasket.Items.Single(item =>
                item.Product.ProductCategory.CategoryDescription == "Head Gear").Product.ProductPrice);
        }

        [Fact(DisplayName = @"Given offer voucher code When the basket value is greater than
            the voucher threshold and it does not contain a qualifying product Then it returns a discounted
            amount that is equal to the basket amount")]
        public async Task BasketIsGreaterThanVoucherThreshold_AndContainsNoValidProduct_DeductNothing()
        {
            var testProductCategoryId = Guid.NewGuid();
            _offerVoucherRepository.Setup(
                v => v.GetOfferVoucherAsync(It.IsAny<string>()))
                    .ReturnsAsync(new OfferVoucherDomainModel
                    {
                        OfferVoucherId = Guid.NewGuid(),
                        OfferVoucherCode = offerVoucherCode,
                        OfferVoucherDescription = "£5.00 off Head Gear in baskets over £50.00",
                        OfferVoucherThresholdValue = offerVoucherThreshold,
                        OfferVoucherValue = offerVoucherAmount,
                        ProductCategory = new ProductCategoryDomainModel
                        {
                            CategoryId = testProductCategoryId,
                            CategoryDescription = "Head Gear"
                        }
                    });

            var testBasket = new ShoppingBasketDomainModel
            {
                Items = new List<ShoppingBasketItemDomainModel>
                {
                    new ShoppingBasketItemDomainModel
                    {
                        Product = new ProductDomainModel
                        {
                            ProductId = Guid.NewGuid(),
                            ProductDescription = "Standard Hat",
                            ProductPrice = 10.00M,
                            ProductCategory = new ProductCategoryDomainModel
                            {
                                CategoryId = Guid.NewGuid(),
                                CategoryDescription = "Hats"
                            }
                        },
                        ProductQuantity = 6
                    }
                },
                BasketTotal = 60.00M,
                BasketDiscountedTotal = 60.00M,
                Status = string.Empty
            };

            var result = await _offerVoucherService.ProcessOfferVoucher(offerVoucherCode, testBasket);

            result.DiscountValue.Should().Be(0);
        }
    }
}
