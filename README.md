# PureGym Shopping Basket

A .Net Core (C#) implementation of a micro-service to handle the creation of a shopping basket. Based on products in the system, you can create a new basket by POST'ing a model of the items you wish to purchase, along with any discount voucher codes you wish to use. The service calculates the total cost of the items you are purchasing and applies any discounts automatically, based on the voucher codes you provide. Validity of the voucher codes provided is also checked by the service before applying, displaying a status message in the basket if any vouchers were deemed invalid.

The repository layer of this service is currently SQLite, which ustilises EF Core as the ORM. Below there are some test models that can be used with a tool like [Postman](https://www.getpostman.com/), to make GET/POST requests to the service, once it is running.

## Database Migration

Before using this service, migrations will need to be run to prepare the SQLite database for use.

- Simply run `Update-Database` in the Package Manager Console

On first build/run of this service, the database will then be seeded with the following entities:

### *Product Categories*

GUID | Description
:--- | :---
**c962832d-dc03-4a04-8bcf-2fd3d7bf41a1** | Hats
**6d2b16e5-9e66-4181-b152-520f3b1c9a13** | Tops
**f9135480-e717-47a5-9fc1-869638785eeb** | Head Gear
**97d7bdd9-0e4a-4f55-95e5-593ea7c149d2** | Gift Vouchers

### *Products*

GUID | Description | Price
:--- | :--- | ---:
**05e3c0f1-6db8-42f2-ba56-aad8ad01a0f5** | Standard Hat | £10.50
**afb9987e-8604-438f-95e8-0bfd9b8cfde0** | Expensive Hat | £25.00
**d164bf2e-7e5d-4efe-b776-c42c5f306fb3** | Standard Jumper | £26.00
**a5b14daf-050a-4a3a-8cb1-252ba1035772** | Expensive Jumper | £54.65
**28763fc8-866a-4c0c-addd-7eeaa2c854f0** | Head Light | £3.50
**5f10bd73-3c1e-4d8f-9435-7c7956e0f973** | Gift Voucher | £30.00

### *Gift Vouchers*

GUID | Code | Value
:--- | :--- | ---:
**9860b642-615f-4063-9c2b-5f14cacefb30** | XXX-XXX | £5.00

### *Offer Vouchers*

GUID | Code | Description
:--- | :--- | :---
**76de2181-04c5-4589-a437-17ef10ba5bfc** | YYY-YYY | £5.00 off Head Gear in baskets over £50.00
**fbedb18a-07a6-4c98-be09-6ffbb52bb8b6** | ZZZ-ZZZ | £5.00 off baskets over £50.00

## Testing

### *Products*

To get all products in the system, make a `GET` request to:

- `https://localhost:44304/api/v1/products/all`

To get a selection of products, make a `GET` request (with the following JSON body) to:

- `https://localhost:44304/api/v1/products/`

```json
["05e3c0f1-6db8-42f2-ba56-aad8ad01a0f5", "afb9987e-8604-438f-95e8-0bfd9b8cfde0"]
```

*This should return both a '**Standard Hat**' and an '**Expensive Hat**'.*

---

### *Gift Vouchers*

To get a gift voucher, make a `GET` request to:

- `https://localhost:44304/api/v1/giftvouchers/XXX-XXX`

*This should return a '**£5.00 value**' gift voucher.*

---

### *Offer Vouchers*

To get an offer voucher, make a `GET` request to:

- `https://localhost:44304/api/v1/offervouchers/YYY-YYY`

*This should return an offer voucher for '**£5.00 off Head Gear in baskets over £50.00**'.*

---

### *Shopping Basket*

**Test 1**

Make a `POST` request (with the following JSON body) to:

- `https://localhost:44304/api/v1/shoppingbasket`

```json
{
	"items": [
		{
			"productid": "05e3c0f1-6db8-42f2-ba56-aad8ad01a0f5",
			"productquantity": 1
		},
		{
			"productid": "a5b14daf-050a-4a3a-8cb1-252ba1035772",
			"productquantity": 1
		}
	],
	"vouchercodes": ["XXX-XXX"],
	"offercode": ""
}
```

*This should return a basket with a total of **£65.15** and a discounted total of **£60.15**. The discounted total shows the amount the basket is valued at **after** voucher deductions.*

**Test 2**

Make a `POST` request (with the following JSON body) to:

- `https://localhost:44304/api/v1/shoppingbasket`

```json
{
	"items": [
		{
			"productid": "afb9987e-8604-438f-95e8-0bfd9b8cfde0",
			"productquantity": 1
		},
		{
			"productid": "d164bf2e-7e5d-4efe-b776-c42c5f306fb3",
			"productquantity": 1
		}
	],
	"vouchercodes": [],
	"offercode": "YYY-YYY"
}
```

*This should return a basket with a total of **£51.00** and a discounted total of **£51.00**. There should also be a status message that states: '**There are no qualifying products in your basket, needed to use voucher 'YYY-YYY'**'*

**Test 3**

Make a `POST` request (with the following JSON body) to:

- `https://localhost:44304/api/v1/shoppingbasket`

```json
{
	"items": [
		{
			"productid": "afb9987e-8604-438f-95e8-0bfd9b8cfde0",
			"productquantity": 1
		},
		{
			"productid": "d164bf2e-7e5d-4efe-b776-c42c5f306fb3",
			"productquantity": 1
		},
		{
			"productid": "28763fc8-866a-4c0c-addd-7eeaa2c854f0",
			"productquantity": 1
		}
	],
	"vouchercodes": [],
	"offercode": "YYY-YYY"
}
```

*This should return a basket with a total of **£54.50** and a discounted total of **£51.00**.*

**Test 4**

Make a `POST` request (with the following JSON body) to:

- `https://localhost:44304/api/v1/shoppingbasket`

```json
{
	"items": [
		{
			"productid": "afb9987e-8604-438f-95e8-0bfd9b8cfde0",
			"productquantity": 1
		},
		{
			"productid": "d164bf2e-7e5d-4efe-b776-c42c5f306fb3",
			"productquantity": 1
		}
	],
	"vouchercodes": ["XXX-XXX"],
	"offercode": "ZZZ-ZZZ"
}
```

*This should return a basket with a total of **£51.00** and a discounted total of **£41.00**.*

**Test 5**

Make a `POST` request (with the following JSON body) to:

- `https://localhost:44304/api/v1/shoppingbasket`

```json
{
	"items": [
		{
			"productid": "afb9987e-8604-438f-95e8-0bfd9b8cfde0",
			"productquantity": 1
		},
		{
			"productid": "5f10bd73-3c1e-4d8f-9435-7c7956e0f973",
			"productquantity": 1
		}
	],
	"vouchercodes": [],
	"offercode": "ZZZ-ZZZ"
}
```

*This should return a basket with a total of **£55.00** and a discounted total of **£55.00**. There should also be a status message that states: '**You have not reached the required spend threshold for voucher ZZZ-ZZZ. You need to spend another £25.01 to receive a £5.00 discount off of your basket total.**'*
