﻿using Xunit;

namespace ShoppingBasket.ComponentTests.Configuration
{
    [CollectionDefinition("SQLite test collection")]
    public class SqliteTestCollection : ICollectionFixture<SqliteTestSetup>
    {
    }
}
