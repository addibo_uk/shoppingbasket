﻿using Microsoft.EntityFrameworkCore;
using System;

namespace ShoppingBasket.ComponentTests.Configuration
{
    public class SqliteTestSetup : SqliteTestBase, IDisposable
    {
        public SqliteTestSetup()
        {
            DestroyDb();
            CreateDb();
        }

        private static void CreateDb()
        {
            using (var context = TestShoppingBasketDbContext())
            {
                context.Database.Migrate();
            }
        }

        private static void DestroyDb()
        {
            using (var context = TestShoppingBasketDbContext())
            {
                context.Database.EnsureDeleted();
            }
        }

        public void Dispose()
        {
            DestroyDb();
        }
    }
}
