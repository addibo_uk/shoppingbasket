﻿using Microsoft.EntityFrameworkCore;
using ShoppingBasket.Data;

namespace ShoppingBasket.ComponentTests.Configuration
{
    public class SqliteTestBase
    {
        protected static ShoppingBasketDbContext TestShoppingBasketDbContext()
        {
            return new ShoppingBasketDbContext(new DbContextOptionsBuilder<ShoppingBasketDbContext>()
                .UseSqlite(TestConstants.ConnectionString)
                .Options);
        }
    }
}
