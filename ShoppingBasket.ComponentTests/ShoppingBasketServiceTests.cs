﻿using FluentAssertions;
using Newtonsoft.Json;
using ShoppingBasket.ComponentTests.Configuration;
using ShoppingBasket.EntityModels;
using ShoppingBasket.InformationModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace ShoppingBasket.ComponentTests
{
    [Collection("SQLite test collection")]
    public class ShoppingBasketSerivceTests : SqliteTestBase, IClassFixture<SqliteTestFactory>
    {
        private readonly SqliteTestFactory _sqliteTestFactory;

        public ShoppingBasketSerivceTests(SqliteTestFactory sqliteTestFactory)
        {
            _sqliteTestFactory = sqliteTestFactory;
        }

        [Fact(DisplayName =
            "Given a valid gift voucher When creating a basket Then the voucher discount value is applied")]
        public async Task GiftVoucher_AppliesVoucherDiscount()
        {
            var testProductOneId = Guid.NewGuid();
            var testProductTwoId = Guid.NewGuid();
            var testGiftVoucherCode = "XXX-XXX";

            var testProducts = new List<ProductEntity>
            {
                new ProductEntity
                {
                    ProductId = testProductOneId,
                    ProductDescription = "Standard Hat",
                    ProductPrice = 10.50M,
                    ProductCategory = new ProductCategoryEntity
                    {
                        CategoryDescription = "Hats"
                    }
                },
                new ProductEntity
                {
                    ProductId = testProductTwoId,
                    ProductDescription = "Expensive Jumper",
                    ProductPrice = 54.65M,
                    ProductCategory = new ProductCategoryEntity
                    {
                        CategoryDescription = "Tops"
                    }
                }
            };

            var testGiftVoucher = new GiftVoucherEntity
            {
                GiftVoucherValue = 5.00M,
                GiftVoucherCode = testGiftVoucherCode
            };

            using (var context = TestShoppingBasketDbContext())
            {
                await context.Products.AddRangeAsync(testProducts);
                await context.GiftVouchers.AddAsync(testGiftVoucher);
                await context.SaveChangesAsync();
            }

            var testBasket = new NewShoppingBasketInformation
            {
                Items = new List<NewShoppingBasketItemInformation>
                {
                    new NewShoppingBasketItemInformation
                    {
                        ProductId = testProductOneId,
                        ProductQuantity = 1
                    },
                    new NewShoppingBasketItemInformation
                    {
                        ProductId = testProductTwoId,
                        ProductQuantity = 1
                    }
                },
                VoucherCodes = new List<string>
                {
                    testGiftVoucherCode
                }
            };

            var response = await _sqliteTestFactory.CreateClient()
                .PostAsJsonAsync($"/api/v1/shoppingbasket", testBasket);
            response.EnsureSuccessStatusCode();

            var content = await response.Content.ReadAsStringAsync();
            var result = JsonConvert.DeserializeObject<ShoppingBasketInformation>(content);

            result.BasketDiscountedTotal.Should().Be(testProducts.Select(product => product.ProductPrice).Sum() - testGiftVoucher.GiftVoucherValue);
            result.BasketTotal.Should().Be(testProducts.Select(product => product.ProductPrice).Sum());
        }

        [Fact(DisplayName =
            "Given an offer voucher When no valid products are in basket Then basket status message is displayed and no discount is applied")]
        public async Task OfferVoucher_NoValidProduct_SetsBasketStatusMessage_NoDiscountApplied()
        {
            var testProductOneId = Guid.NewGuid();
            var testProductTwoId = Guid.NewGuid();
            var testOfferVoucherCode = "YYY-YYY";

            var testProducts = new List<ProductEntity>
            {
                new ProductEntity
                {
                    ProductId = testProductOneId,
                    ProductDescription = "Expensive Hat",
                    ProductPrice = 25.00M,
                    ProductCategory = new ProductCategoryEntity
                    {
                        CategoryDescription = "Hats"
                    }
                },
                new ProductEntity
                {
                    ProductId = testProductTwoId,
                    ProductDescription = "Standard Jumper",
                    ProductPrice = 26.00M,
                    ProductCategory = new ProductCategoryEntity
                    {
                        CategoryDescription = "Tops"
                    }
                }
            };

            var testOfferVoucher = new OfferVoucherEntity
            {
                OfferVoucherValue = 5.00M,
                OfferVoucherCode = testOfferVoucherCode,
                OfferVoucherThresholdValue = 50.00M,
                ProductCategory = new ProductCategoryEntity
                {
                    CategoryDescription = "Head Gear"
                }
            };

            using (var context = TestShoppingBasketDbContext())
            {
                await context.Products.AddRangeAsync(testProducts);
                await context.OfferVouchers.AddAsync(testOfferVoucher);
                await context.SaveChangesAsync();
            }

            var testBasket = new NewShoppingBasketInformation
            {
                Items = new List<NewShoppingBasketItemInformation>
                {
                    new NewShoppingBasketItemInformation
                    {
                        ProductId = testProductOneId,
                        ProductQuantity = 1
                    },
                    new NewShoppingBasketItemInformation
                    {
                        ProductId = testProductTwoId,
                        ProductQuantity = 1
                    }
                },
                OfferCode = testOfferVoucherCode
            };

            var response = await _sqliteTestFactory.CreateClient()
                .PostAsJsonAsync($"/api/v1/shoppingbasket", testBasket);
            response.EnsureSuccessStatusCode();

            var content = await response.Content.ReadAsStringAsync();
            var result = JsonConvert.DeserializeObject<ShoppingBasketInformation>(content);

            result.BasketDiscountedTotal.Should().Be(result.BasketTotal);
            result.Status.Should().Be($"There are no qualifying products in your basket, needed to use voucher '{testOfferVoucher.OfferVoucherCode}'");
        }

        [Fact(DisplayName =
            "Given an offer voucher When valid basket threshold value and qualifying product present Then the offer voucher discount is applied")]
        public async Task OfferVoucher_ValidBasketThreshold_QualifyingProduct_AppliesVoucherDiscount()
        {
            var testProductOneId = Guid.NewGuid();
            var testProductTwoId = Guid.NewGuid();
            var testProductThreeId = Guid.NewGuid();
            var testProductCategoryCode = Guid.NewGuid();
            var testOfferVoucherCode = "ZZZ-ZZZ";

            var testProducts = new List<ProductEntity>
            {
                new ProductEntity
                {
                    ProductId = testProductOneId,
                    ProductDescription = "Expensive Hat",
                    ProductPrice = 25.00M,
                    ProductCategory = new ProductCategoryEntity
                    {
                        CategoryDescription = "Hats"
                    }
                },
                new ProductEntity
                {
                    ProductId = testProductTwoId,
                    ProductDescription = "Standard Jumper",
                    ProductPrice = 26.00M,
                    ProductCategory = new ProductCategoryEntity
                    {
                        CategoryDescription = "Tops"
                    }
                },
                new ProductEntity
                {
                    ProductId = testProductThreeId,
                    ProductDescription = "Head Light",
                    ProductPrice = 3.50M,
                    ProductCategory = new ProductCategoryEntity
                    {
                        CategoryDescription = "Head Gear"
                    }
                }
            };

            var testOfferVoucher = new OfferVoucherEntity
            {
                OfferVoucherValue = 5.00M,
                OfferVoucherCode = testOfferVoucherCode,
                OfferVoucherThresholdValue = 50.00M,
                ProductCategory = testProducts[2].ProductCategory
            };

            using (var context = TestShoppingBasketDbContext())
            {
                await context.Products.AddRangeAsync(testProducts);
                await context.OfferVouchers.AddAsync(testOfferVoucher);
                await context.SaveChangesAsync();
            }

            var testBasket = new NewShoppingBasketInformation
            {
                Items = new List<NewShoppingBasketItemInformation>
                {
                    new NewShoppingBasketItemInformation
                    {
                        ProductId = testProductOneId,
                        ProductQuantity = 1
                    },
                    new NewShoppingBasketItemInformation
                    {
                        ProductId = testProductTwoId,
                        ProductQuantity = 1
                    },
                    new NewShoppingBasketItemInformation
                    {
                        ProductId = testProductThreeId,
                        ProductQuantity = 1
                    }
                },
                OfferCode = testOfferVoucherCode
            };

            var response = await _sqliteTestFactory.CreateClient()
                .PostAsJsonAsync($"/api/v1/shoppingbasket", testBasket);
            response.EnsureSuccessStatusCode();

            var content = await response.Content.ReadAsStringAsync();
            var result = JsonConvert.DeserializeObject<ShoppingBasketInformation>(content);

            result.BasketDiscountedTotal.Should().Be(testProducts.Select(product => product.ProductPrice).Sum() - testProducts[2].ProductPrice);
            result.BasketTotal.Should().Be(testProducts.Select(product => product.ProductPrice).Sum());
        }

        [Fact(DisplayName =
            "Given an offer voucher and a valid gift voucher When valid basket threshold value Then the offer voucher and gift voucher discounts are applied")]
        public async Task OfferVoucherAndGiftVoucher_ValidBasketThreshold_AppliesOfferAndGiftVoucherDiscounts()
        {
            var testProductOneId = Guid.NewGuid();
            var testProductTwoId = Guid.NewGuid();
            var testProductCateoryCode = Guid.NewGuid();
            var testOfferVoucherCode = "AAA-AAA";
            var testGiftVoucherCode = "BBB-BBB";

            var testProducts = new List<ProductEntity>
            {
                new ProductEntity
                {
                    ProductId = testProductOneId,
                    ProductDescription = "Expensive Hat",
                    ProductPrice = 25.00M,
                    ProductCategory = new ProductCategoryEntity
                    {
                        CategoryDescription = "Hats"
                    }
                },
                new ProductEntity
                {
                    ProductId = testProductTwoId,
                    ProductDescription = "Standard Jumper",
                    ProductPrice = 26.00M,
                    ProductCategory = new ProductCategoryEntity
                    {
                        CategoryDescription = "Tops"
                    }
                }
            };

            var testOfferVoucher = new OfferVoucherEntity
            {
                OfferVoucherValue = 5.00M,
                OfferVoucherCode = testOfferVoucherCode,
                OfferVoucherThresholdValue = 50.00M
            };

            var testGiftVoucher = new GiftVoucherEntity
            {
                GiftVoucherValue = 5.00M,
                GiftVoucherCode = testGiftVoucherCode
            };

            using (var context = TestShoppingBasketDbContext())
            {
                await context.Products.AddRangeAsync(testProducts);
                await context.OfferVouchers.AddAsync(testOfferVoucher);
                await context.GiftVouchers.AddAsync(testGiftVoucher);
                await context.SaveChangesAsync();
            }

            var testBasket = new NewShoppingBasketInformation
            {
                Items = new List<NewShoppingBasketItemInformation>
                {
                    new NewShoppingBasketItemInformation
                    {
                        ProductId = testProductOneId,
                        ProductQuantity = 1
                    },
                    new NewShoppingBasketItemInformation
                    {
                        ProductId = testProductTwoId,
                        ProductQuantity = 1
                    }
                },
                OfferCode = testOfferVoucherCode,
                VoucherCodes = new List<string>
                {
                    testGiftVoucherCode
                }
            };

            var response = await _sqliteTestFactory.CreateClient()
                .PostAsJsonAsync($"/api/v1/shoppingbasket", testBasket);
            response.EnsureSuccessStatusCode();

            var content = await response.Content.ReadAsStringAsync();
            var result = JsonConvert.DeserializeObject<ShoppingBasketInformation>(content);

            result.BasketDiscountedTotal.Should()
                .Be(testProducts.Select(product => product.ProductPrice).Sum() - testOfferVoucher.OfferVoucherValue - testGiftVoucher.GiftVoucherValue);
            result.BasketTotal.Should().Be(testProducts.Select(product => product.ProductPrice).Sum());
        }

        [Fact(DisplayName =
            "Given an offer voucher When the basket threshold value is not met as it contains a gift voucher purchase Then basket status message is displayed and no discount is applied")]
        public async Task OfferVoucher_BasketThresholdNotMet_SetsBasketStatusMessage_NoDiscountApplied()
        {
            var testProductOneId = Guid.NewGuid();
            var testProductTwoId = Guid.NewGuid();
            var testOfferVoucherCode = "CCC-CCC";

            var testProducts = new List<ProductEntity>
            {
                new ProductEntity
                {
                    ProductId = testProductOneId,
                    ProductDescription = "Expensive Hat",
                    ProductPrice = 25.00M,
                    ProductCategory = new ProductCategoryEntity
                    {
                        CategoryDescription = "Hats"
                    }
                },
                new ProductEntity
                {
                    ProductId = testProductTwoId,
                    ProductDescription = "Gift Voucher",
                    ProductPrice = 30.00M,
                    ProductCategory = new ProductCategoryEntity
                    {
                        CategoryDescription = "Gift Vouchers"
                    }
                }
            };

            var testOfferVoucher = new OfferVoucherEntity
            {
                OfferVoucherValue = 5.00M,
                OfferVoucherCode = testOfferVoucherCode,
                OfferVoucherThresholdValue = 50.00M
            };

            using (var context = TestShoppingBasketDbContext())
            {
                await context.Products.AddRangeAsync(testProducts);
                await context.OfferVouchers.AddAsync(testOfferVoucher);
                await context.SaveChangesAsync();
            }

            var testBasket = new NewShoppingBasketInformation
            {
                Items = new List<NewShoppingBasketItemInformation>
                {
                    new NewShoppingBasketItemInformation
                    {
                        ProductId = testProductOneId,
                        ProductQuantity = 1
                    },
                    new NewShoppingBasketItemInformation
                    {
                        ProductId = testProductTwoId,
                        ProductQuantity = 1
                    }
                },
                OfferCode = testOfferVoucherCode
            };

            var response = await _sqliteTestFactory.CreateClient()
                .PostAsJsonAsync($"/api/v1/shoppingbasket", testBasket);
            response.EnsureSuccessStatusCode();

            var content = await response.Content.ReadAsStringAsync();
            var result = JsonConvert.DeserializeObject<ShoppingBasketInformation>(content);

            result.BasketDiscountedTotal.Should().Be(result.BasketTotal);
            result.Status.Should().Be($"You have not reached the required spend threshold for voucher {testOfferVoucherCode}. " +
                $"You need to spend another £{string.Format("{0:0.00}", (testOfferVoucher.OfferVoucherThresholdValue - testProducts[0].ProductPrice) + 0.01M)} " +
                $"to receive a £{string.Format("{0:0.00}", testOfferVoucher.OfferVoucherValue)} discount off of your basket total.");
        }
    }
}
